package com.fiedi.compassapp

import android.content.Context
import android.widget.Toast
import com.fiedi.compassapp.validator.CoordinateType
import com.fiedi.compassapp.validator.CoordinateValidator
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(Toast::class)
class CoordinateValidatorTest {

    private val LONGITUDE_MIN = -90
    private val LONGITUDE_MAX = 90

    private val LATITUDE_MIN = 0
    private val LATITUDE_MAX = 180

    private val coordinateValidator = CoordinateValidator()
    private val context = mock(Context::class.java)
    private val toast = mock(Toast::class.java)

    @Before
    fun mockToast() {
        PowerMockito.mockStatic(Toast::class.java)
        `when`(Toast.makeText(ArgumentMatchers.any(Context::class.java), ArgumentMatchers.anyString(),ArgumentMatchers.anyInt())).thenReturn(toast)
    }

    @Test
    fun emptyStringShouldReturnFalse() {
        assertEquals(false, exampleCoordinateToValidate(""))
        verify(toast, times(1)).show()
    }

    @Test
    fun nonNumericStringShouldReturnFalse() {
        assertEquals(false, exampleCoordinateToValidate("12g"))
        verify(toast, times(1)).show()
    }

    @Test
    fun tooLowLatitudeShouldReturnFalse() {
        assertEquals(false, exampleCoordinateToValidate((LATITUDE_MIN-0.000001).toString(), CoordinateType.LATITUDE))
        verify(toast, times(1)).show()
    }

    @Test
    fun tooBigLatitudeShouldReturnFalse() {
        assertEquals(false, exampleCoordinateToValidate((LATITUDE_MAX+0.000001).toString(), CoordinateType.LATITUDE))
        verify(toast, times(1)).show()
    }

    @Test
    fun tooLowLongitudeShouldReturnFalse() {
        assertEquals(false, exampleCoordinateToValidate((LONGITUDE_MIN-0.000001).toString(), CoordinateType.LONGITUTDE))
        verify(toast, times(1)).show()
    }

    @Test
    fun tooBigLongitudeShouldReturnFalse() {
        assertEquals(false, exampleCoordinateToValidate((LONGITUDE_MAX+0.000001).toString(), CoordinateType.LONGITUTDE))
        verify(toast, times(1)).show()
    }

    @Test
    fun validLongitudeShouldReturnTrue() {
        assertEquals(true, exampleCoordinateToValidate((LONGITUDE_MAX).toString(), CoordinateType.LONGITUTDE))
        verify(toast, times(0)).show()
    }

    @Test
    fun validLatitudeShouldReturnTrue() {
        assertEquals(true, exampleCoordinateToValidate((LATITUDE_MAX).toString(), CoordinateType.LATITUDE))
        verify(toast, times(0)).show()
    }

    private fun exampleCoordinateToValidate(content: String) : Boolean {
        return coordinateValidator.validate(CoordinateType.LATITUDE, content, context)
    }

    private fun exampleCoordinateToValidate(content: String, type: CoordinateType) : Boolean {
        return coordinateValidator.validate(type, content, context)
    }

}
