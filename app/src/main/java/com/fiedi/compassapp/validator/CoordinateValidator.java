package com.fiedi.compassapp.validator;

import android.content.Context;
import android.widget.Toast;

public class CoordinateValidator {

    private static int LONGITUDE_MIN = -90;
    private static int LONGITUDE_MAX = 90;

    private static int LATITUDE_MIN = 0;
    private static int LATITUDE_MAX = 180;

    private static String LONGITUDE_NAME = "Longitude";
    private static String LATITUDE_NAME = "Latitude";

    public boolean validate(CoordinateType coordinateType, String coordinateValue, Context context) {

        if(coordinateValue.isEmpty()) {
            Toast.makeText(context, "Should provide " + coordinateType.getCoordinateName(), Toast.LENGTH_SHORT).show();
            return false;
        } else
        if(!isNumeric(coordinateValue)) {
            Toast.makeText(context, coordinateType.getCoordinateName() + " should be float number", Toast.LENGTH_SHORT).show();
            return false;
        } else
        if(coordinateType == coordinateType.LONGITUTDE) {
            if (!isInRange(LONGITUDE_MIN,LONGITUDE_MAX, Double.valueOf(coordinateValue))) {
                Toast.makeText(context, coordinateType.getCoordinateName() + " range is " + LONGITUDE_MIN + " to " + LONGITUDE_MAX, Toast.LENGTH_SHORT).show();
                return false;
            }
        } else
        if(coordinateType == coordinateType.LATITUDE) {
            if (!isInRange(LATITUDE_MIN,LATITUDE_MAX, Double.valueOf(coordinateValue))) {
                Toast.makeText(context, coordinateType.getCoordinateName() + " range is " + LATITUDE_MIN + " to " + LATITUDE_MAX, Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    private boolean isInRange(int min, int max,Double value) {
        return (value <= max &&  value >= min);
    }

    public static boolean isNumeric(String str) {
        try {
            Float.parseFloat(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

}
