package com.fiedi.compassapp.validator

enum class CoordinateType(val coordinateName: String) {

    LATITUDE("Latitude"),
    LONGITUTDE("Longitude")
}