package com.fiedi.compassapp

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fiedi.compassapp.validator.CoordinateType
import com.fiedi.compassapp.validator.CoordinateValidator
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin

private const val PERMISSION_REQUEST = 10

class MainActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var sensorManager: SensorManager
    private lateinit var locationManager: LocationManager
    private var coordinateValidator: CoordinateValidator = CoordinateValidator()

    private var mGravity = FloatArray(3)
    private var mGeoMagnetic = FloatArray(3)

    private var azimuth = 0f
    private var currentAzimuth = 0f

    private var pointAzimuth = 0f
    private var currentPointAzimuth = 0f

    private var chosenLatitude = 0.0
    private var chosenLongitude = 0.0

    private var hasGps = false
    private var hasNetwork = false
    private var locationGps: Location? = null
    private var locationNetwork: Location? = null

    private lateinit var currentLocation: Location
    private var locationPermissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initSensors()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission(locationPermissions)) {
                getLocation()
            } else {
                requestPermissions(locationPermissions, PERMISSION_REQUEST)
            }
        } else {
            getLocation()
        }
    }

    private fun initSensors() {
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL)
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL)

        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
            when (accuracy) {
                0 -> println("Unreliable")
                1 -> println("Low Accuracy")
                2 -> println("Medium Accuracy")
                3 -> println("High Accuracy")
            }
        }

    override fun onSensorChanged(event: SensorEvent?) {
        synchronized(this) {
            when(event?.sensor?.type) {
                Sensor.TYPE_ACCELEROMETER -> {
                    fillMatrix(mGravity,event)
                }
                Sensor.TYPE_MAGNETIC_FIELD -> {
                    fillMatrix(mGeoMagnetic,event)
                }
            }

            var R = FloatArray(9)
            var I = FloatArray(9)

            var success : Boolean = SensorManager.getRotationMatrix(R,I,mGravity,mGeoMagnetic)
            if (success) {
                var orientation = FloatArray(3)
                SensorManager.getOrientation(R,orientation)

                //Compass Animation
                azimuth = Math.toDegrees(orientation[0].toDouble()).toFloat()
                azimuth = (azimuth+360)%360
                compassTemplate.startAnimation(generateAnimation(currentAzimuth,azimuth))
                currentAzimuth = azimuth

                if(arrow.visibility == View.VISIBLE) {
                    //Arrow Animation
                    pointAzimuth = azimuth
                    pointAzimuth -= bearing(currentLocation!!.latitude, currentLocation!!.longitude, chosenLatitude, chosenLongitude).toFloat()
                    arrow.startAnimation(generateAnimation(currentPointAzimuth,pointAzimuth))
                    currentPointAzimuth = pointAzimuth
                }
            }
        }
    }

    fun showCoordinates(view: View) {
        var valid : Boolean = true
        if(coordinateValidator.validate(CoordinateType.LATITUDE,latitude.text.toString(), this) && valid) {
            chosenLatitude = Location.convert(latitude.text.toString())
        } else {
            valid = false
        }
        if(coordinateValidator.validate(CoordinateType.LONGITUTDE,longitude.text.toString(), this) && valid) {
                chosenLongitude = Location.convert(longitude.text.toString())
        } else {
            valid = false
        }
        if(valid) {
            arrow.visibility = View.VISIBLE
        }
    }

    fun clearCoordinates(view: View) {
        longitude.text.clear()
        latitude.text.clear()
        if(arrow.animation != null) {
            arrow.animation.cancel()
        }
        arrow.visibility=View.INVISIBLE
    }

    private fun generateAnimation(pCurrentAzimuth: Float, pAzimuth: Float) : RotateAnimation {
        var animation = RotateAnimation(-pCurrentAzimuth,-pAzimuth, Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f)

        animation.duration = 0
        animation.repeatCount = Animation.ABSOLUTE
        animation.fillAfter = true
        return animation
    }

    private fun bearing(startLat: Double, startLng: Double, endLat: Double, endLng: Double): Double {
        val latitude1 = Math.toRadians(startLat)
        val latitude2 = Math.toRadians(endLat)
        val longDiff = Math.toRadians(endLng - startLng)
        val y = sin(longDiff) * cos(latitude2)
        val x = cos(latitude1) * sin(latitude2) - sin(latitude1) * cos(latitude2) * cos(longDiff)
        return (Math.toDegrees(atan2(y, x)) + 360) % 360
    }

    private fun fillMatrix (matrix: FloatArray,event: SensorEvent) {
        val alpha : Float = 0.97f
        for(i in 0..2){ matrix[i] = alpha * matrix[i] + (1-alpha)*event.values[i] }
    }

    //LOCATION FUNCTIONS

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        hasGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        hasNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        if (hasGps || hasNetwork) {

            if (hasGps) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0F, object : LocationListener {

                    override fun onLocationChanged(location: Location) {
                        if (location != null) {
                            locationGps = location
                        }
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                    }

                    override fun onProviderEnabled(provider: String) {

                    }

                    override fun onProviderDisabled(provider: String) {

                    }

                })

                val localGpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                if (localGpsLocation != null)
                    locationGps = localGpsLocation
            }
            if (hasNetwork) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0F, object : LocationListener {
                    override fun onLocationChanged(location: Location) {
                        if (location != null) {
                            locationNetwork = location
                        }
                    }
                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                    }

                    override fun onProviderEnabled(provider: String) {

                    }

                    override fun onProviderDisabled(provider: String) {

                    }

                })

                val localNetworkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                if (localNetworkLocation != null)
                    locationNetwork = localNetworkLocation
            }

            if(locationGps!= null && locationNetwork!= null){
                if(locationGps!!.accuracy > locationNetwork!!.accuracy){
                    currentLocation = locationNetwork!!
                }else{
                    currentLocation = locationGps!!
                }
            }

        } else {
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
    }

    //    private fun getLocation() {
    //        currentLocation = locationHandler.getLocation(locationManager)
    //        if(currentLocation == null) {
    //            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
    //        }
    //    }

    private fun checkPermission(permissionArray: Array<String>): Boolean {
        var allSuccess = true
        for (i in permissionArray.indices) {
            if (checkCallingOrSelfPermission(permissionArray[i]) == PackageManager.PERMISSION_DENIED)
                allSuccess = false
        }
        return allSuccess
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST) {
            var allSuccess = true
            for (i in permissions.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    allSuccess = false
                    val requestAgain = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && shouldShowRequestPermissionRationale(permissions[i])
                    if (requestAgain) {
                        Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Go to settings and enable the permission", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            if (allSuccess)
                getLocation()
        }
    }

}